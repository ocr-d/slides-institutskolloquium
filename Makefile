.PHONY: all

all: presentation.pdf

%.pdf: %.tex
	lualatex $*
