#set term qt enhanced size 1280, 768
set term pngcairo font "Sans, 14" nocrop enhanced size 1280, 768
set output "plot-cer.png"

set title '{/:Italic OCR post-correction results with corFST, corRNN, and corRNN+corFST}' font "Sans,22"

ocrs = "Fraktur deu-frak foo fraktur fraktur-jze"
cols = "OCR corFST.trainlex corFST.testlex corRNN corRNN+corFST.trainlex corRNN+corFST.testlex"

set grid

set ylabel "CER [%]"
set boxwidth 1.0 abs
set xtics scale 0 ()
set for [i=1:5] xtics add (word(ocrs,i) 3+(i-1)*7)

set key right top Left # center

plot 'plot-cer.data' index 0 using (0):1 with boxes fillstyle solid 0.3 linecolor 4 title columnhead(1), \
     '' index 0 using (1):2 with boxes fillstyle solid 0.3 linecolor 1 title columnhead(2), \
     '' index 0 using (2):3 with boxes fillstyle pattern 2 linecolor 1 title columnhead(3), \
     '' index 0 using (3):4 with boxes fillstyle solid 0.3 linecolor 2 title columnhead(4), \
     '' index 0 using (4):5 with boxes fillstyle solid 0.3 linecolor 3 title columnhead(5), \
     '' index 0 using (5):6 with boxes fillstyle pattern 2 linecolor 3 title columnhead(6), \
     '' index 1 using (7):1 with boxes fillstyle solid 0.3 linecolor 4 notitle, \
     '' index 1 using (8):2 with boxes fillstyle solid 0.3 linecolor 1 notitle, \
     '' index 1 using (9):3 with boxes fillstyle pattern 2 linecolor 1 notitle, \
     '' index 1 using (10):4 with boxes fillstyle solid 0.3 linecolor 2 notitle, \
     '' index 1 using (11):5 with boxes fillstyle solid 0.3 linecolor 3 notitle, \
     '' index 1 using (12):6 with boxes fillstyle pattern 2 linecolor 3 notitle, \
     '' index 2 using (14):1 with boxes fillstyle solid 0.3 linecolor 4 notitle, \
     '' index 2 using (15):2 with boxes fillstyle solid 0.3 linecolor 1 notitle, \
     '' index 2 using (16):3 with boxes fillstyle pattern 2 linecolor 1 notitle, \
     '' index 2 using (17):4 with boxes fillstyle solid 0.3 linecolor 2 notitle, \
     '' index 2 using (18):5 with boxes fillstyle solid 0.3 linecolor 3 notitle, \
     '' index 2 using (19):6 with boxes fillstyle pattern 2 linecolor 3 notitle, \
     '' index 3 using (21):1 with boxes fillstyle solid 0.3 linecolor 4 notitle, \
     '' index 3 using (22):2 with boxes fillstyle solid 0.3 linecolor 1 notitle, \
     '' index 3 using (23):3 with boxes fillstyle pattern 2 linecolor 1 notitle, \
     '' index 3 using (24):4 with boxes fillstyle solid 0.3 linecolor 2 notitle, \
     '' index 3 using (25):5 with boxes fillstyle solid 0.3 linecolor 3 notitle, \
     '' index 3 using (26):6 with boxes fillstyle pattern 2 linecolor 3 notitle, \
     '' index 4 using (28):1 with boxes fillstyle solid 0.3 linecolor 4 notitle, \
     '' index 4 using (29):2 with boxes fillstyle solid 0.3 linecolor 1 notitle, \
     '' index 4 using (30):3 with boxes fillstyle pattern 2 linecolor 1 notitle, \
     '' index 4 using (31):4 with boxes fillstyle solid 0.3 linecolor 2 notitle, \
     '' index 4 using (32):5 with boxes fillstyle solid 0.3 linecolor 3 notitle, \
     '' index 4 using (33):6 with boxes fillstyle pattern 2 linecolor 3 notitle
