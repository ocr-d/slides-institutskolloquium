#set term qt enhanced size 1280, 768
set term pngcairo font "Sans, 14" nocrop enhanced size 1280, 768
set output "plot-roc.png"
#set term pdfcairo font "Sans, 14" nocrop enhanced size 1280, 768
#set output "plot-roc.pdf"

set multiplot title '{/:Italic corFST: influence of rejection threshold, before/after combination with corRNN}' font "Sans,22" layout 1,2

set grid


set xlabel "rejection weight"
set ylabel "quality measure"
set xrange [0:5]
set key right bottom # center

plot 'plot-roc.data' index 0 using 1:(1-$4) with lines dashtype 1 linecolor 1 lw 2 title "accuracy corFST",\
     '' index 0 using 1:2 with lines dashtype 2 linecolor 1 lw 2 title "precision corFST",\
     '' index 0 using 1:3 with lines dashtype 3 linecolor 1 lw 2 title "recall corFST",\
     '' index 0 using 1:2:3 with filledcurves above fillstyle transparent solid 0.3 linecolor 1 notitle,\
     '' index 0 using 1:2:3 with filledcurves below fillstyle pattern 2 linecolor 1 notitle,\
     '' index 1 using 1:(1-$4) with lines dashtype 1 linecolor 2 lw 2 title "accuracy corRNN",\
     '' index 1 using 1:2 with lines dashtype 2 linecolor 2 lw 2 title "precision corRNN",\
     '' index 1 using 1:3 with lines dashtype 3 linecolor 2 lw 2 title "recall corRNN",\
     '' index 2 using 1:(1-$4) with lines dashtype 1 linecolor 3 lw 2 title "accuracy corRNN+corFST",\
     '' index 2 using 1:2 with lines dashtype 2 linecolor 3 lw 2 title "precision corRNN+corFST",\
     '' index 2 using 1:3 with lines dashtype 3 linecolor 3 lw 2 title "recall corRNN+corFST",\
     '' index 2 using 1:2:3 with filledcurves above fillstyle transparent solid 0.3 linecolor 3 notitle,\
     '' index 2 using 1:2:3 with filledcurves below fillstyle pattern 2 linecolor 3 notitle


set xlabel "false positive rate"
set ylabel "true positive rate"
set xrange [0:0.1]
set yrange [0:1]

# receiver operating characteristic:
plot 'plot-roc.data' index 0 using 6:5 with linespoints pointtype 5 linecolor 1 lw 2 title 'ROC corFST', \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 5::0::4 with labels textcolor ls 1 offset char 0,-1 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 5::1::4 with labels textcolor ls 1 offset char 3,-1 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 5::2::4 with labels textcolor ls 1 offset char 6,-1 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 5::3::4 with labels textcolor ls 1 offset char 9,-1 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 5::4::4 with labels textcolor ls 1 offset char 12,-1 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 1::5::10 with labels textcolor ls 1 offset char 3,0 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 4::11 with labels textcolor ls 1 offset char 3,0.5 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 4::12 with labels textcolor ls 1 offset char 6,0.5 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 4::13 with labels textcolor ls 1 offset char 10,0.5 notitle, \
     '' index 0 using 6:5:(sprintf("%.1f", $1)) every 4::14 with labels textcolor ls 1 offset char 14,0.5 notitle, \
     '' index 1 using 6:5 with point pointsize 3 linecolor 2 lw 2 title 'ROC corRNN', \
     '' index 2 using 6:5 with linespoints pointtype 5 linecolor 3 lw 2 title 'ROC corRNN+corFST', \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::0::4 with labels textcolor ls 3 offset char 3,0.1 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::1::4 with labels textcolor ls 3 offset char 6,0.1 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::2::4 with labels textcolor ls 3 offset char 9,0.1 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::3::4 with labels textcolor ls 3 offset char 12,0.1 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::4::4 with labels textcolor ls 3 offset char 15,0.1 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 1::5::8 with labels textcolor ls 3 offset char -3,0 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 1::9::9 with labels textcolor ls 3 offset char -3,0.1 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::10 with labels textcolor ls 3 offset char 0,0.5 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::11 with labels textcolor ls 3 offset char 3,0.5 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::12 with labels textcolor ls 3 offset char 6,0.5 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::13 with labels textcolor ls 3 offset char 10,0.5 notitle, \
     '' index 2 using 6:5:(sprintf("%.1f", $1)) every 5::14 with labels textcolor ls 3 offset char 14,0.5 notitle

unset multiplot

